import sys, math, random
argv = sys.argv

codingfile = argv[1]
noncodingfile = argv[2]
humseqfile = argv[3]

# Parse Fastas
seqs = []

with open(codingfile) as fasta:
	for line in fasta:
		if line.startswith('>'):
			continue
		else:
			line = line.replace('N', random.choice('ACTG'))
			seqs.append(line.rstrip())

nonseq = ''

with open(noncodingfile) as fasta:
	for line in fasta:
		line = line.rstrip()
		line = line.replace(' ', '')

		if line.startswith('>'):
			continue

		if len(line) == 0:
			continue
		else:
			nonseq = nonseq + line

humanseq = ''

with open(humseqfile) as fasta:
	for line in fasta:
		if line.startswith('>'):
			continue
		else:
			humanseq = humanseq + line.rstrip()

# Create Markov Indices
mchain = {}

for seq in seqs:
	front = 0
	back = 6
	frame = seq[front:back]
	pentaframe = frame[0:5]

	while back <= len(seq):
		if pentaframe in mchain:
			mchain[pentaframe][frame] += 1
			mchain[pentaframe]['total'] += 1
		else:
			mchain[pentaframe] = {pentaframe+'A': 0, pentaframe+'C': 0, pentaframe+'T': 0, pentaframe+'G': 0, 'total':0}
			mchain[pentaframe][frame] += 1
			mchain[pentaframe]['total'] += 1

		front += 1
		back += 1
		frame = seq[front:back]
		pentaframe = frame[0:5]


mchainnon = {}

counter = 0
front = 0
back = 6
frame = seq[front:back]
pentaframe = frame[0:5]

while back <= len(nonseq):
	if pentaframe in mchainnon:
		mchainnon[pentaframe][frame] += 1
		mchainnon[pentaframe]['total'] += 1
	else:
		mchainnon[pentaframe] = {pentaframe+'A': 0, pentaframe+'C': 0, pentaframe+'T': 0, pentaframe+'G': 0, 'total':0}
		mchainnon[pentaframe][frame] += 1
		mchainnon[pentaframe]['total'] += 1

	front += 1
	back += 1
	frame = nonseq[front:back]
	pentaframe = frame[0:5]

# Create Reverse Sequence
f = open('revseq.txt', 'w')
revseq = ''

for i in humanseq:
	if i == 'A':
		revseq = revseq + 'T'
	elif i == 'C':
		revseq = revseq + 'G'
	elif i == 'T':
		revseq = revseq + 'A'
	elif i == 'G':
		revseq = revseq + 'C'

f.write(revseq)
f.close()

#Coding Differential Test
front = 0
back = 6
prexon = 0
printron = 0
frame = humanseq[front:back]
pentaframe = frame[0:5]

while back <= len(humanseq):
	if pentaframe in mchain:
		prexon += (float(mchain[pentaframe][frame])/float(mchain[pentaframe]['total']))

	if pentaframe in mchainnon:
		printron += (float(mchainnon[pentaframe][frame])/float(mchainnon[pentaframe]['total']))

	front += 1
	back += 1
	frame = humanseq[front:back]
	pentaframe = frame[0:5]
	diff = prexon/printron
	codediff = math.log(diff, 2)


print codediff

front = 0
back = 6
prexon = 0
printron = 0
frame = revseq[front:back]
pentaframe = frame[0:5]

while back <= len(revseq):
	if pentaframe in mchain:
		prexon += (float(mchain[pentaframe][frame])/float(mchain[pentaframe]['total']))

	if pentaframe in mchainnon:
		printron += (float(mchainnon[pentaframe][frame])/float(mchainnon[pentaframe]['total']))

	front += 1
	back += 1
	frame = revseq[front:back]
	pentaframe = frame[0:5]
	diff = prexon/printron
	codediff = math.log(diff, 2)

print codediff


